/*
 * Copyright (c) 2015-2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/***** Includes *****/
/* XDCtools Header files */
#include <SensorTask.h>
#include <sensors/bme280.h>
#include <xdc/runtime/System.h>
#include <xdc/std.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>

/* TI-RTOS Header files */
#include <ti/devices/DeviceFamily.h>
#include <ti/display/Display.h>
#include <ti/display/DisplayExt.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/PIN.h>
#include <ti/devices/cc13x0/driverlib/cpu.h>

/* Board Header files */
#include "Board.h"

/* Application Header files */
#include "NodeRadioTask.h"
#include "RadioProtocol.h"
#include "drivers/i2c_driver.h"
#include "sensors/opt3001.h"
#include "sensors/tmp007.h"

/***** Defines *****/
#define NODE_TASK_STACK_SIZE 1024
#define NODE_TASK_PRIORITY 3

#define SENSOR_REPORT_INTERVAL_MS   1000

/***** Variable declarations *****/
static Task_Params nodeTaskParams;
Task_Struct nodeTask; /* Not static so you can see in ROV */
static uint8_t nodeTaskStack[NODE_TASK_STACK_SIZE];

/* Pin driver handle */
static PIN_Handle ledPinHandle;
static PIN_State ledPinState;

/* Display driver handles */
static Display_Handle hDisplaySerial;

/* Enable the 3.3V power domain used by the LCD */

PIN_Config pinTable[] = {
#if !defined Board_CC1350STK
                         NODE_ACTIVITY_LED | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
#endif
                         PIN_TERMINATE};

/*
 * Application button pin configuration table:
 *   - Buttons interrupts are configured to trigger on falling edge.
 */
PIN_Config buttonPinTable[] = {
                               Board_PIN_BUTTON0 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
                               PIN_TERMINATE};
uint16_t rawTemp;
uint16_t rawObjTemp;
uint16_t rawLux;

uint8_t bmp208Buffer[8];
uint8_t* p_bmp208Buffer = bmp208Buffer;
int32_t bmp208Temp;
uint32_t bmp208Pressure;
uint32_t bmp208Humidity;

sensorPacket_t sensorPayload;
sensorPacket_t * pSensorPayload = &sensorPayload;
/***** Prototypes *****/
static void sensorTaskFunction(UArg arg0, UArg arg1);

/***** Function definitions *****/
void sensorTask_init(void) {

    /* Create the node task */
    Task_Params_init(&nodeTaskParams);
    nodeTaskParams.stackSize = NODE_TASK_STACK_SIZE;
    nodeTaskParams.priority = NODE_TASK_PRIORITY;
    nodeTaskParams.stack = &nodeTaskStack;
    Task_construct(&nodeTask, sensorTaskFunction, &nodeTaskParams, NULL);
}

static void sensorTaskFunction(UArg arg0, UArg arg1) {
    /* Initialize display and try to open both UART and LCD types of display. */
    Display_Params params;
    Display_Params_init(&params);
    params.lineClearMode = DISPLAY_CLEAR_BOTH;

    /* Open both an available LCD display and an UART display.
     * Whether the open call is successful depends on what is present in the
     * Display_config[] array of the board file.
     *
     * Note that for SensorTag evaluation boards combined with the SHARP96x96
     * Watch DevPack, there is a pin conflict with UART such that one must be
     * excluded, and UART is preferred by default. To display on the Watch
     * DevPack, add the precompiler define BOARD_DISPLAY_EXCLUDE_UART.
     */
    hDisplaySerial = Display_open(Display_Type_UART, &params);

    /* Check if the selected Display type was found and successfully opened */
    if (hDisplaySerial) {
        Display_printf(hDisplaySerial, 0, 0, "Waiting for SCE ADC reading...");
    }

    /* Open LED pins */
    ledPinHandle = PIN_open(&ledPinState, pinTable);
    if (!ledPinHandle) {
        System_abort("Error initializing board 3.3V domain pins\n");
    }

    /* Start SensorBoosterPack */
    initI2C();

    Display_printf(hDisplaySerial, 0, 0, "\033[2J \033[0;0HStart Sensors Setup Process!!");

    sensorOpt3001Init();
    sensorOpt3001Enable(true);
    bme280_init();
    bme280_enable(true);


    while (1) {

        PIN_setOutputValue(ledPinHandle, NODE_ACTIVITY_LED,!PIN_getOutputValue(NODE_ACTIVITY_LED));

        sensorTmp007Read(&rawTemp, &rawObjTemp);
        sensorOpt3001Read(&rawLux);

        bme280_read(p_bmp208Buffer);
        bme280_convert(p_bmp208Buffer, &bmp208Temp, &bmp208Pressure, &bmp208Humidity);

        pSensorPayload->rawAmbientTemp = rawTemp;
        pSensorPayload->rawObjectTemp = rawObjTemp;
        pSensorPayload->rawLight = rawLux;
        pSensorPayload->rawHumidity = bmp208Humidity;
        pSensorPayload->rawPressure = bmp208Pressure;

        /* Send ADC value to concentrator */
        NodeRadioTask_sendAdcData(&sensorPayload);
        Display_printf(hDisplaySerial, 0, 0, "Sensors Data Sent!");

        CPUdelay(SENSOR_REPORT_INTERVAL_MS * 10000);
    }
}
