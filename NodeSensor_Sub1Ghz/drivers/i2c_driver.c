/*
 * i2c_driver.c
 *
 *  Created on: May 23, 2020
 *      Author: Alex
 */

#include "i2c_driver.h"


uint8_t buffer[32];

I2C_Transaction initI2C() {
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;
    i2c = I2C_open(Board_I2C0, &i2cParams);

    return i2cTransaction;
}

bool writeI2C(uint8_t ui8Addr, uint8_t ui8Reg, uint8_t *writeData, uint8_t ui8ByteCount) {
    uint8_t i;
    uint8_t *p = buffer;

    /* Copy address and data to local buffer for burst write */
    *p++ = ui8Reg;
    for (i = 0; i < ui8ByteCount; i++)
    {
        *p++ = *writeData++;
    }
    ui8ByteCount++;

    /* Send data */
    i2cTransaction.writeCount   = ui8ByteCount;
    i2cTransaction.writeBuf     = &buffer;
    i2cTransaction.readCount    = 2;
    i2cTransaction.readBuf      = rxBuffer;
    i2cTransaction.slaveAddress = ui8Addr;

    return I2C_transfer(i2c, &i2cTransaction) == true;
}

bool readI2C(uint8_t ui8Addr, uint8_t ui8Reg, uint8_t *readData, uint8_t ui8ByteCount) {

    I2C_Transaction masterTransaction;
    txBuffer[0] = ui8Reg;
    masterTransaction.writeCount = 1;
    masterTransaction.writeBuf = txBuffer;
    masterTransaction.readCount = ui8ByteCount;
    masterTransaction.readBuf = readData;
    masterTransaction.slaveAddress = ui8Addr;

    bool i2cTransferResult = I2C_transfer(i2c, &masterTransaction);


//    txBuffer[0] = ui8Reg;
//    txBuffer[1] = 0;
//
//    i2cTransaction.writeBuf   = txBuffer;
//    i2cTransaction.readCount = ui8ByteCount;
//    i2cTransaction.readBuf   = rxBuffer;
//    i2cTransaction.slaveAddress = ui8Addr;
//    bool i2cTransferResult = I2C_transfer(i2c, &i2cTransaction);
//
//    if (!i2cTransferResult) {
//        // Read Failed
//        return false;
//    }
//
//    uint8_t i = 0;
//    for (i = 0; i < ui8ByteCount; i++) {
//        Data[i] = rxBuffer[i];
//    }

    return true;
}





