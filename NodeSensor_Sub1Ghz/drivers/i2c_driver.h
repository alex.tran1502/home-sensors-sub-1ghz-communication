/*
 * i2c_driver.h
 *
 *  Created on: May 23, 2020
 *      Author: Alex
 */

#ifndef DRIVERS_I2C_DRIVER_H_
#define DRIVERS_I2C_DRIVER_H_
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <ti/drivers/I2C.h>


/* Board Header files */
#include "Board.h"

I2C_Handle      i2c;
I2C_Params      i2cParams;
I2C_Transaction i2cTransaction;
uint8_t         txBuffer[5];
uint8_t         rxBuffer[5];

extern I2C_Transaction initI2C();
extern bool writeI2C(uint8_t ui8Addr, uint8_t ui8Reg, uint8_t *writeData, uint8_t ui8ByteCount);
extern bool readI2C(uint8_t ui8Addr, uint8_t ui8Reg, uint8_t *readData, uint8_t ui8ByteCount);

#endif /* DRIVERS_I2C_DRIVER_H_ */
