# Home sensor network with sub 1GHZ

## OTA Sensor Packet Format
* [0] (1 byte): Source Address
* [1] (1 byte): Packet Type
* [2:3] (2 bytes): Light 
* [4:5] (2 bytes): Ambient Temperature
* [6:7] (2 bytes): Target Object Temperature
* [8:9] (2 bytes): Battery Level
* [10:13] (4 bytes): Pressure Level
* [14:17] (4 bytes): Humidity Level
* [18:21] (4 bytes): Time