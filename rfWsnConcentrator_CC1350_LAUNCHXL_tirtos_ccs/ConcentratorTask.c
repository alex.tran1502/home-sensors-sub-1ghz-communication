/******************************************************************************

 @file ConcentratorTask.c

 @brief Easylink Concentrator Example Application

 Group: CMCU LPRF
 Target Device: cc13x0

 ******************************************************************************

 Copyright (c) 2015-2019, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************


 *****************************************************************************/

/***** Includes *****/
#include <stdio.h>
#include <string.h>
/* XDCtools Header files */
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <xdc/std.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>

/* TI-RTOS Header files */
#include <ti/devices/DeviceFamily.h>
#include <ti/display/Display.h>
#include <ti/display/DisplayExt.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/UART.h>
#include DeviceFamily_constructPath(driverlib/cpu.h)

/* Board Header files */
#include "Board.h"

/* Application Header files */
#include "ConcentratorRadioTask.h"
#include "ConcentratorTask.h"
#include "RadioProtocol.h"
#include "drivers/i2c_driver.h"
#include "sensors/bme280.h"
#include "sensors/opt3001.h"
#include "sensors/tmp007.h"

/***** Defines *****/
#define CONCENTRATOR_TASK_STACK_SIZE 1024
#define CONCENTRATOR_TASK_PRIORITY 3

#define CONCENTRATOR_EVENT_ALL 0xFFFFFFFF
#define CONCENTRATOR_EVENT_NEW_ADC_SENSOR_VALUE (uint32_t)(1 << 0)
#define CONCENTRAROR_EVENT_UPDATE_SERIAL (uint32_t)(2 << 0)
#define CONCENTRATOR_MAX_NODES 7

#define CONCENTRATOR_DISPLAY_LINES 8

#define CONCENTRATOR_LED_BLINK_ON_DURATION_MS 100
#define CONCENTRATOR_LED_BLINK_OFF_DURATION_MS 400
#define CONCENTRATOR_LED_BLINK_TIMES 5

#define CONCENTRATOR_IDENTIFY_LED Board_PIN_LED1

/***** Type declarations *****/
struct AdcSensorNode {
    uint8_t address;

    uint16_t rawLight;
    uint16_t rawAmbientTemp;
    uint16_t rawObjectTemp;
    uint32_t rawPressure;
    uint32_t rawHumidity;

    float realLight;
    float realAmbientTemp;
    float realObjectTemp;
    float realPressure;
    float realHumidity;

    int8_t latestRssi;
};

/***** Variable declarations *****/
static Task_Params concentratorTaskParams;
Task_Struct concentratorTask; /* not static so you can see in ROV */
static uint8_t concentratorTaskStack[CONCENTRATOR_TASK_STACK_SIZE];
Event_Struct concentratorEvent; /* not static so you can see in ROV */
static Event_Handle concentratorEventHandle;
static struct AdcSensorNode latestActiveAdcSensorNode;
struct AdcSensorNode knownSensorNodes[CONCENTRATOR_MAX_NODES];
static struct AdcSensorNode* lastAddedSensorNode = knownSensorNodes;
static Display_Handle hDisplayLcd;
static Display_Handle hDisplaySerial;

uint16_t rawTemp;
uint16_t rawObjTemp;
float realAmbTemp;
float realObjTemp;

uint16_t rawLux;
float realLux;

uint8_t bmp208Buffer[8];
char uart_tx_buffer[256];
uint8_t* p_bmp208Buffer = bmp208Buffer;
float bmp208Temp;
float bmp208Pressure;
float bmp208Humidity;

/* Pin driver handle */
static PIN_Handle identifyLedPinHandle;
static PIN_State identifyLedPinState;

/* Configure LED Pin */
PIN_Config identifyLedPinTable[] = {
                                    CONCENTRATOR_IDENTIFY_LED | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
                                    PIN_TERMINATE};

/* Clock for sensor stub */
Clock_Struct ledBlinkClock; /* Not static so you can see in ROV */
Clock_Struct uartCommClock;
static Clock_Handle ledBlinkClockHandle;
static Clock_Handle uartCommClockHandle;
static uint8_t ledBlinkCnt;


/***** Prototypes *****/
static void concentratorTaskFunction(UArg arg0, UArg arg1);
static void packetReceivedCallback(union ConcentratorPacket* packet, int8_t rssi);
static void updateSerial(void);
static void addNewNode(struct AdcSensorNode* node);
static void updateNode(struct AdcSensorNode* node);
static uint8_t isKnownNodeAddress(uint8_t address);
static void ledBlinkClockCb(UArg arg0);
void uartClockHandler();
/***** Function definitions *****/
void ConcentratorTask_init(void) {
    /* Create event used internally for state changes */
    Event_Params eventParam;
    Event_Params_init(&eventParam);
    Event_construct(&concentratorEvent, &eventParam);
    concentratorEventHandle = Event_handle(&concentratorEvent);

    /* Create the concentrator radio protocol task */
    Task_Params_init(&concentratorTaskParams);
    concentratorTaskParams.stackSize = CONCENTRATOR_TASK_STACK_SIZE;
    concentratorTaskParams.priority = CONCENTRATOR_TASK_PRIORITY;
    concentratorTaskParams.stack = &concentratorTaskStack;
    Task_construct(&concentratorTask, concentratorTaskFunction, &concentratorTaskParams, NULL);

    /* Open Identify LED pin */
    identifyLedPinHandle = PIN_open(&identifyLedPinState, identifyLedPinTable);
    if (!identifyLedPinHandle) {
        System_abort("Error initializing board 3.3V domain pins\n");
    }

    /* Create Identify Clock to Blink LED */
    Clock_Params clkParams;
    Clock_Params_init(&clkParams);

    clkParams.startFlag = FALSE;
    Clock_construct(&ledBlinkClock, ledBlinkClockCb, 1, &clkParams);
    ledBlinkClockHandle = Clock_handle(&ledBlinkClock);

    ledBlinkCnt = 0;

    /* Create Uart Communication Clock */
    Clock_Params uartClkParams;
    Clock_Params_init(&uartClkParams);

    uartClkParams.period = 100000;
    uartClkParams.startFlag = FALSE;
    Clock_construct(&uartCommClock, uartClockHandler, 1, &uartClkParams);
    uartCommClockHandle = Clock_handle(&uartCommClock);
}

static void concentratorTaskFunction(UArg arg0, UArg arg1) {
    /* Initialize display and try to open both UART and LCD types of display. */
    Display_Params params;
    Display_Params_init(&params);
    params.lineClearMode = DISPLAY_CLEAR_BOTH;

    hDisplayLcd = Display_open(Display_Type_LCD, &params);
    hDisplaySerial = Display_open(Display_Type_UART, &params);

    /* Check if the selected Display type was found and successfully opened */
    if (hDisplaySerial) {
        Display_printf(hDisplaySerial, 0, 0, "Waiting for nodes...");
    }

    /* Check if the selected Display type was found and successfully opened */
    if (hDisplayLcd) {
        Display_printf(hDisplayLcd, 0, 0, "Waiting for nodes...");
    }

    initI2C();
    sensorOpt3001Init();
    sensorOpt3001Enable(true);
    bme280_init();
    bme280_enable(true);
    /* Register a packet received callback with the radio task */
    ConcentratorRadioTask_registerPacketReceivedCallback(packetReceivedCallback);

    /* Enter main task loop */
    Clock_start(uartCommClockHandle);
    while (1) {
        /* Wait for event */
        uint32_t events = Event_pend(concentratorEventHandle, 0, CONCENTRATOR_EVENT_ALL, BIOS_WAIT_FOREVER);

        /* If we got a new ADC sensor value */
        if (events & CONCENTRATOR_EVENT_NEW_ADC_SENSOR_VALUE) {
            /* If we knew this node from before, update the value */
            if (isKnownNodeAddress(latestActiveAdcSensorNode.address)) {
                updateNode(&latestActiveAdcSensorNode);
            } else {
                /* Else add it */
                addNewNode(&latestActiveAdcSensorNode);
            }
        }

        if (events & CONCENTRAROR_EVENT_UPDATE_SERIAL) {
            /* Update the values on the LCD */
            updateSerial();
        }
    }
}

static void packetReceivedCallback(union ConcentratorPacket* packet, int8_t rssi) {
    /* If we recived an DualMode ADC sensor packet*/
    if (packet->header.packetType == RADIO_PACKET_TYPE_DM_SENSOR_PACKET) {
        /* Save the values */
        latestActiveAdcSensorNode.address = packet->header.sourceAddress;
        latestActiveAdcSensorNode.rawLight = packet->dmSensorPacket.rawLight;
        latestActiveAdcSensorNode.rawAmbientTemp = packet->dmSensorPacket.rawAmbientTemp;
        latestActiveAdcSensorNode.rawObjectTemp = packet->dmSensorPacket.rawObjectTemp;
        latestActiveAdcSensorNode.rawPressure = packet->dmSensorPacket.rawPressure;
        latestActiveAdcSensorNode.rawHumidity = packet->dmSensorPacket.rawHumidity;

        sensorTmp007Convert(latestActiveAdcSensorNode.rawAmbientTemp, latestActiveAdcSensorNode.rawObjectTemp, 
                            &latestActiveAdcSensorNode.realObjectTemp, &latestActiveAdcSensorNode.realAmbientTemp);
        sensorOpt3001Convert(latestActiveAdcSensorNode.rawLight, &latestActiveAdcSensorNode.realLight);
        latestActiveAdcSensorNode.realHumidity = latestActiveAdcSensorNode.rawHumidity / 1024.0;
        latestActiveAdcSensorNode.realPressure = latestActiveAdcSensorNode.rawPressure;

        latestActiveAdcSensorNode.latestRssi = rssi;

        Event_post(concentratorEventHandle, CONCENTRATOR_EVENT_NEW_ADC_SENSOR_VALUE);
    }
}

static uint8_t isKnownNodeAddress(uint8_t address) {
    uint8_t found = 0;
    uint8_t i;
    for (i = 0; i < CONCENTRATOR_MAX_NODES; i++) {
        if (knownSensorNodes[i].address == address) {
            found = 1;
            break;
        }
    }
    return found;
}

static void updateNode(struct AdcSensorNode* node) {
    uint8_t i;
    for (i = 0; i < CONCENTRATOR_MAX_NODES; i++) {
        if (knownSensorNodes[i].address == node->address) {

            knownSensorNodes[i].rawLight = node->rawLight;
            knownSensorNodes[i].rawAmbientTemp = node->rawAmbientTemp;
            knownSensorNodes[i].rawObjectTemp = node->rawObjectTemp;
            knownSensorNodes[i].rawPressure = node->rawPressure;
            knownSensorNodes[i].rawHumidity = node->rawHumidity;
            knownSensorNodes[i].latestRssi = node->latestRssi;

            knownSensorNodes[i].realLight = node->realLight;
            knownSensorNodes[i].realAmbientTemp = node->realAmbientTemp;
            knownSensorNodes[i].realObjectTemp = node->realObjectTemp;
            knownSensorNodes[i].realPressure = node->realPressure;
            knownSensorNodes[i].realHumidity = node->realHumidity;

            break;
        }
    }
}

static void addNewNode(struct AdcSensorNode* node) {
    *lastAddedSensorNode = *node;

    /* Increment and wrap */
    lastAddedSensorNode++;
    if (lastAddedSensorNode > &knownSensorNodes[CONCENTRATOR_MAX_NODES - 1]) {
        lastAddedSensorNode = knownSensorNodes;
    }
}

void uartClockHandler() {
    Event_post(concentratorEventHandle, CONCENTRAROR_EVENT_UPDATE_SERIAL);
}

static void updateSerial(void) {
    struct AdcSensorNode* nodePointer = knownSensorNodes;
    uint8_t currentLcdLine;

    /* Start on the second line */
    currentLcdLine = 1;

    /* Get concentrator sensor value */
    sensorTmp007Read(&rawTemp, &rawObjTemp);
    sensorTmp007Convert(rawTemp, rawObjTemp, &realObjTemp, &realAmbTemp);

    sensorOpt3001Read(&rawLux);
    sensorOpt3001Convert(rawLux, &realLux);

    bme280_read(p_bmp208Buffer);
    bme280_convert(p_bmp208Buffer, &bmp208Temp, &bmp208Pressure, &bmp208Humidity);



    /* Write one line per node */
    while ((nodePointer < &knownSensorNodes[CONCENTRATOR_MAX_NODES]) &&
            (nodePointer->address != 0)) {


        Display_printf(hDisplaySerial, 0, 0, "{\"node\":\"0x%02x\",\"light\":%f,\"object_temp\":%f,\"ambient_temp\":%f,\"humidity\":%f,\"pressure\":%f}",
                       nodePointer->address,
                       nodePointer->realLight,
                       nodePointer->realObjectTemp,
                       nodePointer->realAmbientTemp,
                       nodePointer->realHumidity,
                       nodePointer->realPressure);

        nodePointer++;
        currentLcdLine++;
    }

    Display_printf(hDisplaySerial, 0, 0, "{\"node\":\"0x00\",\"light\":%f,\"object_temp\":%f,\"ambient_temp\":%f,\"humidity\":%f,\"pressure\":%f}",
                   realLux,
                   realObjTemp,
                   realAmbTemp,
                   bmp208Humidity,
                   bmp208Pressure);
}

static void ledBlinkClockCb(UArg arg0) {
    if (ledBlinkCnt < CONCENTRATOR_LED_BLINK_TIMES) {
        uint32_t ledState = PIN_getOutputValue(CONCENTRATOR_IDENTIFY_LED);

        if (ledState) {
            ledBlinkCnt++;

            /* turn off LED */
            PIN_setOutputValue(identifyLedPinHandle, CONCENTRATOR_IDENTIFY_LED, 0);

            /* Setup timeout to turn LED on */
            Clock_setTimeout(ledBlinkClockHandle,
                             CONCENTRATOR_LED_BLINK_OFF_DURATION_MS * 1000 / Clock_tickPeriod);

            /* Start sensor stub clock */
            Clock_start(ledBlinkClockHandle);
        } else {
            /* turn on LED */
            PIN_setOutputValue(identifyLedPinHandle, CONCENTRATOR_IDENTIFY_LED, 1);

            /* Setup timeout to turn LED off */
            Clock_setTimeout(ledBlinkClockHandle,
                             CONCENTRATOR_LED_BLINK_ON_DURATION_MS * 1000 / Clock_tickPeriod);

            /* Start sensor stub clock */
            Clock_start(ledBlinkClockHandle);
        }
    } else {
        PIN_setOutputValue(identifyLedPinHandle, CONCENTRATOR_IDENTIFY_LED, 0);
        ledBlinkCnt = 0;
    }
}
