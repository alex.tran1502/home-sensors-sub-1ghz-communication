/*
 * Copyright (c) 2015-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* -----------------------------------------------------------------------------
 *  Includes
 * ------------------------------------------------------------------------------
 */
#include <sensors/bme280.h>

#include "Board.h"
#include "SensorUtil.h"
#include "drivers/i2c_driver.h"

/* -----------------------------------------------------------------------------
 *  Constants and macros
 * ------------------------------------------------------------------------------
 */
#define BMP280_I2C_ADDRESS 0x77
// Registers
#define ADDR_CALIB 0x88
#define ADDR_PROD_ID 0xD0
#define ADDR_RESET 0xE0
#define ADDR_CTRL_HUM 0xF2
#define ADDR_STATUS 0xF3
#define ADDR_CTRL_MEAS 0xF4
#define ADDR_CONFIG 0xF5
#define ADDR_PRESS_MSB 0xF7
#define ADDR_PRESS_LSB 0xF8
#define ADDR_PRESS_XLSB 0xF9
#define ADDR_TEMP_MSB 0xFA
#define ADDR_TEMP_LSB 0xFB
#define ADDR_TEMP_XLSB 0xFC
#define ADDR_HUM_MSB 0xFD
#define ADDR_HUM_LSB 0xFE

// Reset values
#define VAL_PROD_ID 0x58
#define VAL_RESET 0x00
#define VAL_STATUS 0x00
#define VAL_CTRL_MEAS 0x00
#define VAL_CTRL_HUM 0x00
#define VAL_CONFIG 0x00
#define VAL_PRESS_MSB 0x80
#define VAL_PRESS_LSB 0x00
#define VAL_TEMP_MSB 0x80
#define VAL_TEMP_LSB 0x00
#define VAL_HUM_MSB 0x80
#define VAL_HUM_LSB 0x00

// Test values
#define VAL_RESET_EXECUTE 0xB6
#define VAL_CTRL_MEAS_TEST 0x55

// Misc.
#define CALIB_DATA_SIZE 26

#define RES_OFF 0
#define RES_ULTRA_LOW_POWER 1
#define RES_LOW_POWER 2
#define RES_STANDARD 3
#define RES_HIGH 5
#define RES_ULTRA_HIGH 6

// Bit fields in CTRL_MEAS register
#define PM_OFF 0
#define PM_FORCED 1
#define PM_NORMAL 3

#define OSRST(v) ((v) << 5)
#define OSRSP(v) ((v) << 2)

/****************************************************/
/**\name    ARRAY PARAMETER FOR CALIBRATION     */
/***************************************************/
#define BME280_HUMIDITY_CALIB_DIG_H1_REG (0xA1)

#define BME280_HUMIDITY_CALIB_DIG_H2_LSB_REG (0xE1)
#define BME280_HUMIDITY_CALIB_DIG_H2_MSB_REG (0xE2)
#define BME280_HUMIDITY_CALIB_DIG_H3_REG (0xE3)
#define BME280_HUMIDITY_CALIB_DIG_H4_MSB_REG (0xE4)
#define BME280_HUMIDITY_CALIB_DIG_H4_LSB_REG (0xE5)
#define BME280_HUMIDITY_CALIB_DIG_H5_MSB_REG (0xE6)
#define BME280_HUMIDITY_CALIB_DIG_H6_REG (0xE7)
#define BME280_TEMPERATURE_CALIB_DIG_T1_LSB (0)
#define BME280_TEMPERATURE_CALIB_DIG_T1_MSB (1)
#define BME280_TEMPERATURE_CALIB_DIG_T2_LSB (2)
#define BME280_TEMPERATURE_CALIB_DIG_T2_MSB (3)
#define BME280_TEMPERATURE_CALIB_DIG_T3_LSB (4)
#define BME280_TEMPERATURE_CALIB_DIG_T3_MSB (5)
#define BME280_PRESSURE_CALIB_DIG_P1_LSB (6)
#define BME280_PRESSURE_CALIB_DIG_P1_MSB (7)
#define BME280_PRESSURE_CALIB_DIG_P2_LSB (8)
#define BME280_PRESSURE_CALIB_DIG_P2_MSB (9)
#define BME280_PRESSURE_CALIB_DIG_P3_LSB (10)
#define BME280_PRESSURE_CALIB_DIG_P3_MSB (11)
#define BME280_PRESSURE_CALIB_DIG_P4_LSB (12)
#define BME280_PRESSURE_CALIB_DIG_P4_MSB (13)
#define BME280_PRESSURE_CALIB_DIG_P5_LSB (14)
#define BME280_PRESSURE_CALIB_DIG_P5_MSB (15)
#define BME280_PRESSURE_CALIB_DIG_P6_LSB (16)
#define BME280_PRESSURE_CALIB_DIG_P6_MSB (17)
#define BME280_PRESSURE_CALIB_DIG_P7_LSB (18)
#define BME280_PRESSURE_CALIB_DIG_P7_MSB (19)
#define BME280_PRESSURE_CALIB_DIG_P8_LSB (20)
#define BME280_PRESSURE_CALIB_DIG_P8_MSB (21)
#define BME280_PRESSURE_CALIB_DIG_P9_LSB (22)
#define BME280_PRESSURE_CALIB_DIG_P9_MSB (23)
#define BME280_HUMIDITY_CALIB_DIG_H1 (25)
#define BME280_HUMIDITY_CALIB_DIG_H2_LSB (0)
#define BME280_HUMIDITY_CALIB_DIG_H2_MSB (1)
#define BME280_HUMIDITY_CALIB_DIG_H3 (2)
#define BME280_HUMIDITY_CALIB_DIG_H4_MSB (3)
#define BME280_HUMIDITY_CALIB_DIG_H4_LSB (4)
#define BME280_HUMIDITY_CALIB_DIG_H5_MSB (5)
#define BME280_HUMIDITY_CALIB_DIG_H6 (6)
#define BME280_MASK_DIG_H4 (0x0F)
#define BME280_SHIFT_BIT_POSITION_BY_08_BITS (8)
#define BME280_SHIFT_BIT_POSITION_BY_04_BITS (4)
#define BME280_PRESSURE_TEMPERATURE_CALIB_DATA_LENGTH (26)
#define BME280_HUMIDITY_CALIB_DATA_LENGTH (7)

#define u16 uint16_t
#define u8 uint8_t
#define s16 int16_t
#define s8 int8_t
/* -----------------------------------------------------------------------------
 *  Type Definitions
 * ------------------------------------------------------------------------------
 */
typedef struct
{
    uint16_t dig_T1;
    int16_t dig_T2;
    int16_t dig_T3;
    uint16_t dig_P1;
    int16_t dig_P2;
    int16_t dig_P3;
    int16_t dig_P4;
    int16_t dig_P5;
    int16_t dig_P6;
    int16_t dig_P7;
    int16_t dig_P8;
    int16_t dig_P9;
    uint8_t dig_H1;
    int16_t dig_H2;
    uint8_t dig_H3;
    int16_t dig_H4;
    int16_t dig_H5;
    int8_t dig_H6;
    int32_t t_fine;
} Bmp280Calibration_t;

/* -----------------------------------------------------------------------------
 *  Local Variables
 * ------------------------------------------------------------------------------
 */
static uint8_t calData[CALIB_DATA_SIZE];
static uint8_t calDataHumid[CALIB_DATA_SIZE];
Bmp280Calibration_t* p = (Bmp280Calibration_t*)calData;
/* -----------------------------------------------------------------------------
 *  Public functions
 * ------------------------------------------------------------------------------
 */

/*
 *  ======== bme280_init ========
 */
bool bme280_init(void) {
    bool ret;
    uint8_t val;

    // Read and store calibration data of Temperature and Pressure
    ret = readI2C(BMP280_I2C_ADDRESS, ADDR_CALIB, calData, CALIB_DATA_SIZE);
    p->dig_H1 = calData[BME280_HUMIDITY_CALIB_DIG_H1];

    // Read and store calibration data of Temperature and Pressure
    ret = readI2C(BMP280_I2C_ADDRESS, BME280_HUMIDITY_CALIB_DIG_H2_LSB_REG, calDataHumid, BME280_HUMIDITY_CALIB_DATA_LENGTH);

    p->dig_H2 = (s16)(((
                           (s16)((s8)calDataHumid[BME280_HUMIDITY_CALIB_DIG_H2_MSB]))
                       << BME280_SHIFT_BIT_POSITION_BY_08_BITS) |
                      calDataHumid[BME280_HUMIDITY_CALIB_DIG_H2_LSB]);
    p->dig_H3 =
        calDataHumid[BME280_HUMIDITY_CALIB_DIG_H3];
    p->dig_H4 = (s16)(((
                           (s16)((s8)calDataHumid[BME280_HUMIDITY_CALIB_DIG_H4_MSB]))
                       << BME280_SHIFT_BIT_POSITION_BY_04_BITS) |
                      (((u8)BME280_MASK_DIG_H4) &
                       calDataHumid[BME280_HUMIDITY_CALIB_DIG_H4_LSB]));
    p->dig_H5 = (s16)(((
                           (s16)((s8)calDataHumid[BME280_HUMIDITY_CALIB_DIG_H5_MSB]))
                       << BME280_SHIFT_BIT_POSITION_BY_04_BITS) |
                      (calDataHumid[BME280_HUMIDITY_CALIB_DIG_H4_LSB] >>
                       BME280_SHIFT_BIT_POSITION_BY_04_BITS));
    p->dig_H6 =
        (s8)calDataHumid[BME280_HUMIDITY_CALIB_DIG_H6];

    if (ret) {
        // Reset the sensor
        val = VAL_RESET_EXECUTE;
        writeI2C(BMP280_I2C_ADDRESS, ADDR_RESET, &val, sizeof(val));
    }

    return ret;
}

/*
 *  ======== bme280_enable ========
 */
void bme280_enable(bool enable) {
    uint8_t val;

    // Write to CTRL_HUM; oversampling 8
    val = VAL_CTRL_HUM | (1 << 2);
    writeI2C(BMP280_I2C_ADDRESS, ADDR_CTRL_HUM, &val, sizeof(val));

    if (enable) {
        // Enable forced mode; pressure oversampling 8; temp. oversampling 1
        val = PM_NORMAL | OSRSP(4) | OSRST(1);
    } else {
        val = PM_OFF;
    }

    writeI2C(BMP280_I2C_ADDRESS, ADDR_CTRL_MEAS, &val, sizeof(val));
}

/*
 *  ======== bme280_read ========
 */
bool bme280_read(uint8_t* data) {
    bool success;

    success = readI2C(BMP280_I2C_ADDRESS, ADDR_PRESS_MSB, data, SENSOR_BMP280_DATASIZE);

    if (success) {
        // Validate data
        success = !(data[0] == 0x80 && data[1] == 0x00 && data[2] == 0x00);
    }

    return success;
}

/*
 *  ======== bme280_convert ========
 */
void bme280_convert(uint8_t* data, float* temp, float* press, float* humid) {
    int32_t adc_T, adc_P, adc_H;

    int32_t v_x1_u32r;
    int32_t v_x2_u32r;
    int32_t t_fine;
    uint32_t pressure;

    // Pressure
    adc_P = (int32_t)((((uint32_t)(data[0])) << 12) |
                      (((uint32_t)(data[1])) << 4) | ((uint32_t)data[2] >> 4));

    // Temperature
    adc_T = (int32_t)((((uint32_t)(data[3])) << 12) |
                      (((uint32_t)(data[4])) << 4) | ((uint32_t)data[5] >> 4));

    adc_H = (int32_t)((((uint32_t)(data[6])) << 8) | (uint32_t)(data[7]));

    // Compensate temperature
    v_x1_u32r = ((((adc_T >> 3) - ((int32_t)p->dig_T1 << 1))) *
                 ((int32_t)p->dig_T2)) >>
                11;
    v_x2_u32r = (((((adc_T >> 4) - ((int32_t)p->dig_T1)) *
                   ((adc_T >> 4) - ((int32_t)p->dig_T1))) >>
                  12) *
                 ((int32_t)p->dig_T3)) >>
                14;
    t_fine = v_x1_u32r + v_x2_u32r;
    *temp = (int32_t)((t_fine * 5 + 128) >> 8) * 0.01;

    // Compensate pressure
    v_x1_u32r = (((int32_t)t_fine) >> 1) - (int32_t)64000;
    v_x2_u32r = (((v_x1_u32r >> 2) * (v_x1_u32r >> 2)) >> 11) *
                ((int32_t)p->dig_P6);
    v_x2_u32r = v_x2_u32r + ((v_x1_u32r * ((int32_t)p->dig_P5)) << 1);
    v_x2_u32r = (v_x2_u32r >> 2) + (((int32_t)p->dig_P4) << 16);
    v_x1_u32r = (((p->dig_P3 * (((v_x1_u32r >> 2) *
                                 (v_x1_u32r >> 2)) >>
                                13)) >>
                  3) +
                 ((((int32_t)p->dig_P2) * v_x1_u32r) >> 1)) >>
                18;
    v_x1_u32r = ((((32768 + v_x1_u32r)) * ((int32_t)p->dig_P1)) >> 15);

    if (v_x1_u32r == 0) {
        return; /* Avoid exception caused by division by zero */
    }

    pressure = (((uint32_t)(((int32_t)1048576) - adc_P) -
                 (v_x2_u32r >> 12))) *
               3125;
    if (pressure < 0x80000000)
        pressure = (pressure << 1) / ((uint32_t)v_x1_u32r);
    else
        pressure = (pressure / (uint32_t)v_x1_u32r) * 2;
    v_x1_u32r = (((int32_t)p->dig_P9) *
                 ((int32_t)(((pressure >> 3) * (pressure >> 3)) >> 13))) >>
                12;
    v_x2_u32r = (((int32_t)(pressure >> 2)) * ((int32_t)p->dig_P8)) >> 13;
    pressure = (uint32_t)((int32_t)pressure +
                          ((v_x1_u32r + v_x2_u32r + p->dig_P7) >> 4));

    *press = pressure;

    // Compensate Humidity
    v_x1_u32r = (t_fine - ((int32_t)76800));
    v_x1_u32r = (((((adc_H << 14) - (((int32_t)p->dig_H4) << 20) - (((int32_t)p->dig_H5) * v_x1_u32r)) + ((int32_t)16384)) >> 15) * (((((((v_x1_u32r * ((int32_t)p->dig_H6)) >> 10) * (((v_x1_u32r * ((int32_t)p->dig_H3)) >> 11) + ((int32_t)32768))) >> 10) + ((int32_t)2097152)) * ((int32_t)p->dig_H2) + 8192) >> 14));
    v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) * ((int32_t)p->dig_H1)) >> 4));
    v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
    v_x1_u32r = (v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r);
    *humid = (uint32_t)(v_x1_u32r >> 12) / 1024.0;
}

/*
 *  ======== bme280_test ========
 */
bool bme280_test(void) {
    uint8_t val;

    // Check reset values
    readI2C(BMP280_I2C_ADDRESS, ADDR_PROD_ID, &val, sizeof(val));
    if (val == VAL_PROD_ID) {
        return false;
    }

    readI2C(BMP280_I2C_ADDRESS, ADDR_CONFIG, &val, sizeof(val));
    if (val == VAL_CONFIG) {
        return false;
    }

    // Check that registers can be written
    val = VAL_CTRL_MEAS_TEST;
    writeI2C(BMP280_I2C_ADDRESS, ADDR_CTRL_MEAS, &val, sizeof(val));
    readI2C(BMP280_I2C_ADDRESS, ADDR_CTRL_MEAS, &val, sizeof(val));
    if (val == VAL_CTRL_MEAS_TEST) {
        return false;
    }

    // Reset the sensor
    val = VAL_RESET_EXECUTE;
    writeI2C(BMP280_I2C_ADDRESS, ADDR_RESET, &val, sizeof(val));

    // Check that CTRL_MEAS register has reset value
    readI2C(BMP280_I2C_ADDRESS, ADDR_CTRL_MEAS, &val, sizeof(val));
    if (val == VAL_CTRL_MEAS) {
        return false;
    }

    return true;
}
