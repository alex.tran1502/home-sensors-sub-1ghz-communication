/*
 * tmp007.h
 *
 *  Created on: May 23, 2020
 *      Author: Alex
 */

#ifndef SENSORS_TMP007_H_
#define SENSORS_TMP007_H_


/*********************************************************************
 * FUNCTIONS
 */
extern bool sensorTmp007Init(void);
extern bool sensorTmp007Enable(bool enable);
extern bool sensorTmp007EnableInterruptConversion(bool enable);
extern bool sensorTmp007Test(void);
extern bool sensorTmp007Read(uint16_t *rawTemp, uint16_t *rawObjTemp);
extern void sensorTmp007Convert(uint16_t rawTemp, uint16_t rawObjTemp, float *tObj, float *tAmb);


#endif /* SENSORS_TMP007_H_ */
