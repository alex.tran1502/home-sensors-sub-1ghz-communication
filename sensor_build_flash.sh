#!/bin/bash
WORKSPACE=$(pwd)
PROJECT_NAME="NodeSensor_Sub1Ghz"
RED="\033[31m"
GREEN="\033[32m"
NORMAL="\033[0;39m"

# Window Flashing
flash_window() {
  C:/ti/uniflash_6.0.0/deskdb/content/TICloudAgent/win/ccs_base/DebugServer/bin/DSLite.exe \
  flash \
  --config=$WORKSPACE/$PROJECT_NAME/targetConfigs/CC1350F128.ccxml \
  -f \
  $WORKSPACE/$PROJECT_NAME/Debug/$PROJECT_NAME.out
}


cd $PROJECT_NAME/Debug
C:/ti/ccs901/ccs/utils/bin/gmake -k -j 8 all -O

if [ $? -eq 0 ]
then
  echo -e "$GREEN Build Success $NORMAL"
  flash_window
  if [ $? -eq 0 ]
  then
    echo -e "$GREEN Flash Success $NORMAL"
    exit 0
  else
    echo -e "$RED Flash Failed $NORMAL"
    exit 1
  fi
else 
  echo -e "$RED Build Failed $NORMAL"
  exit 1
fi

